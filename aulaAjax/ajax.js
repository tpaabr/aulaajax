const $ = document
const url = "https://in-api-treinamento.herokuapp.com/posts"
const urlDel = "https://in-api-treinamento.herokuapp.com/posts/"
//Adcionar ID para del
const but = $.querySelector("#botao");
const loadBut = $.querySelector("#carregar")
const listaMensagem = $.querySelector("#listaMensagem");

function activateFetch() {

    fetch(url).then((resp) => resp.json()).then((resp) => {
        let cor2 = 0
        for (let i = 0; i < resp.length; i++) {
            
            let currentObj = resp[i];
            let li = $.createElement("li");
            let li2 = $.createElement("li");
    
            let currentName = currentObj.name
            let currentMessage = currentObj.message

            //mudar cor
            cor2 += (255/resp.length)


            cor2 = `#${parseInt(cor2).toString(16)}3333`
            console.log(cor2)
            li.style.backgroundColor = cor2
            li2.style.backgroundColor = cor2
    
            li.innerText = `Nome : ${currentName}`;
            li2.innerText = `Mensagem : ${currentMessage}`;
    
            listaMensagem.appendChild(li);
            listaMensagem.appendChild(li2);
        }
    })
}


const myHeaders = {
    "content-Type":"application/json"
}

but.addEventListener("click", () => {
    let nome = $.querySelector("#nome").value;
    let mensagem = $.querySelector("#mensagem").value;

    let novoPost = {
        "post": {
            "name": nome,
            "message": mensagem
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(novoPost)

    }

    fetch(url, fetchConfig).then(console.log)

})

loadBut.addEventListener("click", () => activateFetch())






